import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:supportui/contants.dart';
import 'package:url_launcher/url_launcher.dart';

class MethodSystem {
  static bool _previousUiAdjustment;

  static void popScreen(BuildContext context) {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  static void openUrl(String url) {
    if (canLaunch(url) != null) {
      launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static void pushToScreen(BuildContext context, Widget screen) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => screen,
        ));
  }

  static void setUiBrightness({Brightness brightness}) {
    if (Platform.isIOS) {
      if (brightness == null && _previousUiAdjustment != null) {
        // SystemChrome.restoreSystemUIOverlays();
        WidgetsBinding.instance.renderView.automaticSystemUiAdjustment =
            _previousUiAdjustment ?? true;
      } else if (brightness != null) {
        _previousUiAdjustment =
            WidgetsBinding.instance.renderView.automaticSystemUiAdjustment;

        WidgetsBinding.instance.renderView.automaticSystemUiAdjustment = false;
        SystemChrome.setSystemUIOverlayStyle(brightness == Brightness.light
            ? SystemUiOverlayStyle.dark
            : SystemUiOverlayStyle.light);
      }
    }
  }

  static Future<String> get userAgent async {
    PackageInfo packageInfo;
    // Package info
    packageInfo = await PackageInfo.fromPlatform();
    final String versionString =
        '${packageInfo.packageName}/${packageInfo.version}+${packageInfo.buildNumber}';

    return versionString;
  }

  // function format date
  static String formatDay({DateTime time, String formatType}) {
    return DateFormat(formatType).format(time);
  }

  // function get date
  static String timeAgo({DateTime time}) {
    if (time != null) {
      final difference = DateTime.now().difference(time);

      if (difference.inDays <= 1) {
        final inDays = difference.inDays;
        if (inDays < 1) {
          if (difference.inHours >= 1) {
            return '${difference.inHours} hours ago';
          } else {
            return 'Now';
          }
        }

        return '1 day ago';
      }
      return formatDay(formatType: UI_DATE_FM, time: time);
    }

    return null;
  }

  // function call to phone number
  static void callPhoneNumber({String number = '+8446248017'}) async {
    //await FlutterPhoneDirectCaller.callNumber(number);
    //launch("tel:" + Uri.encodeComponent('$number'));
    //launch("tel://21213123123");
    //launch('tel: $number');
    if (await canLaunch('tel:$number')) {
      launch('tel:$number');
    }
  }

  // Function Rate App
  static void rateApp({BuildContext context}) async {
    RateMyApp rateMyApp = RateMyApp(
      preferencesPrefix: 'rateMyApp_',
      minDays: 0, // Show rate popup on first day of install.
      minLaunches:
          5, // Show rate popup after 5 launches of app after minDays is passed.
    );
    rateMyApp.init().then((value) {
      rateMyApp.showRateDialog(
        context,
        title: 'titleRating',
        message: 'messageRating',
        rateButton: 'rateButton',
        laterButton: 'rateLate',
        noButton: 'noThanks',
      );
    });
  }
}

class MethodService {
  R lookup<R>(Map map, Iterable keys) {
    dynamic current = map;

    for (final key in keys) {
      if (current is Map) {
        current = current[key];
      } else if (key is int && current is Iterable && current.length > key) {
        current = current.elementAt(key);
      } else {
        return null;
      }
    }

    try {
      if (current is! R) {
        switch (R) {
          case DateTime:
            current = DateTime.tryParse(current?.toString() ?? '')?.toLocal();
            break;
          case num:
            current = num.tryParse(current?.toString() ?? '');
            break;
          case String:
            current = current?.toString();
            break;
        }
      }

      return current as R;
    } on dynamic catch (_) {
      //AppError(message: e.toString());
    }

    // return null as default
    return null;
  }

  static dynamic getDataFromFileJson(BuildContext context,
      {String pathAssetsFile = 'assets/data.json'}) async {
    // assets:
    // - assets/data.json
    String data =
        await DefaultAssetBundle.of(context).loadString(pathAssetsFile);
    return json.decode(data);
  }
}
