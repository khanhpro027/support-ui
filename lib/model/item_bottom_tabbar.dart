import 'package:flutter/material.dart';

class ItemBottomTabbar {
  const ItemBottomTabbar({this.icon, this.text});
  final IconData icon;
  final String text;
}
