import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:supportui/model/item_bottom_tabbar.dart';
import 'package:supportui/widgets/bottom_tabbar_widget.dart';
import 'package:supportui/widgets/example_widgets/book_mark.dart';
import 'package:supportui/widgets/example_widgets/home.dart';

class BottomNavigationBarWidget extends StatefulWidget {
  BottomNavigationBarWidget({Key key}) : super(key: key);

  @override
  _BottomNavigationBarWidgetState createState() =>
      _BottomNavigationBarWidgetState();
}

class _BottomNavigationBarWidgetState extends State<BottomNavigationBarWidget> {
  int _currentPage = 0;

  List<Widget> _bottomBarWidgets = [Home(), BookMark()];

  _selectedTab(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Scaffold(
        body: Builder(
          builder: (context) => IndexedStack(
            children: _bottomBarWidgets,
            index: _currentPage,
          ),
        ),
        // bottomNavigationBar
        bottomNavigationBar: BottomTabbarWidget(
          selectedColor: Theme.of(context).accentColor,
          notchedShape: CircularNotchedRectangle(),
          onTabSelected: _selectedTab,
          items: [
            ItemBottomTabbar(icon: Icons.home, text: 'Home'),
            ItemBottomTabbar(icon: Icons.star, text: 'Book mark'),
          ],
        ),
      ),
    );
  }
}
