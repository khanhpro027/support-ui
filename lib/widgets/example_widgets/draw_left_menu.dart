import 'package:expandable_group/expandable_group_widget.dart';
import 'package:flutter/material.dart';

class ZoneList {
  const ZoneList({this.zoneId, this.name, this.shortUrl, this.childZone});

  final int zoneId;
  final String name;
  final String shortUrl;
  final List<ZoneList> childZone;

  factory ZoneList.fromJson(Map<String, dynamic> json) => ZoneList(
        zoneId: json["Id"],
        name: json["Name"],
        shortUrl: json["ShortURL"],
      );

  Map<String, dynamic> toJson() => {
        "Id": zoneId,
        "Name": name,
        "ShortURL": shortUrl,
      };
}

class SideMenuWidget extends StatelessWidget {
  final List<ZoneList> cates = [
    ZoneList(
      zoneId: 3,
      name: "Thời sự",
      childZone: [
        ZoneList(zoneId: 87, name: 'Bình luận'),
        ZoneList(zoneId: 89, name: 'Phóng sự'),
        ZoneList(zoneId: 200003, name: 'Xã hội'),
        ZoneList(zoneId: 1579, name: 'Bút Bi'),
      ],
    ),
    ZoneList(
      zoneId: 2,
      name: "Thế giới",
      childZone: [
        ZoneList(zoneId: 312, name: 'Kiều bào'),
        ZoneList(zoneId: 442, name: 'Muôn màu'),
        ZoneList(zoneId: 94, name: 'Bình luận'),
        ZoneList(zoneId: 20, name: 'Hồ sơ'),
      ],
    ),
    ZoneList(
      zoneId: 6,
      name: "Pháp luật",
      childZone: [
        ZoneList(zoneId: 200005, name: 'Pháp lý'),
        ZoneList(zoneId: 79, name: 'Tư vấn'),
        ZoneList(zoneId: 266, name: 'Chuyện pháp đình'),
      ],
    ),
    ZoneList(
      zoneId: 11,
      name: "Kinh doanh",
      childZone: [
        ZoneList(zoneId: 775, name: 'Doanh nghiệp'),
        ZoneList(zoneId: 86, name: 'Tài chính'),
        ZoneList(zoneId: 200006, name: 'Mua sắm'),
        ZoneList(zoneId: 200007, name: 'Đầu tư'),
      ],
    ),
    ZoneList(
      zoneId: 200029,
      name: "Công nghệ",
      childZone: [
        ZoneList(zoneId: 67, name: 'Thủ thuật'),
        ZoneList(zoneId: 16, name: 'Nhịp sống số'),
        ZoneList(zoneId: 297, name: 'Nhịp cầu'),
        ZoneList(zoneId: 547, name: 'Thị trường'),
        ZoneList(zoneId: 557, name: 'Trải nghiệm'),
        ZoneList(zoneId: 200028, name: 'Gia đình số'),
      ],
    ),
    ZoneList(
      zoneId: 659,
      name: "Xe",
      childZone: [],
    ),
    ZoneList(
      zoneId: 100,
      name: "Du lịch",
      childZone: [
        ZoneList(zoneId: 100014, name: 'Box video'),
        ZoneList(zoneId: 100099, name: 'Phòng vé'),
        ZoneList(zoneId: 100001, name: 'Cơ hội du lịch'),
        ZoneList(zoneId: 100002, name: 'Nhà hàng'),
        ZoneList(zoneId: 100006, name: 'Khách sạn'),
        ZoneList(zoneId: 100007, name: 'Công ty du lịch'),
        ZoneList(zoneId: 100008, name: 'Món ngon'),
        ZoneList(zoneId: 100009, name: 'Đi chơi'),
        ZoneList(zoneId: 100010, name: 'Ăn gì'),
        ZoneList(zoneId: 100011, name: 'Quê hương'),
        ZoneList(zoneId: 384, name: 'Mách bạn'),
        ZoneList(zoneId: 495, name: 'Box ảnh'),
      ],
    ),
    ZoneList(
      zoneId: 7,
      name: "Nhịp sống trẻ",
      childZone: [
        ZoneList(zoneId: 269, name: 'Việc làm'),
        ZoneList(zoneId: 194, name: 'Yêu'),
        ZoneList(zoneId: 200012, name: 'Xu hướng'),
        ZoneList(zoneId: 200013, name: 'Khám phá'),
        ZoneList(zoneId: 200014, name: 'Nhân vật'),
        ZoneList(zoneId: 1580, name: 'Theo Gương Bác'),
      ],
    ),
    ZoneList(
      zoneId: 200017,
      name: "Văn hóa",
      childZone: [
        ZoneList(zoneId: 200018, name: 'Đời sống'),
        ZoneList(zoneId: 61, name: 'Sách'),
        ZoneList(zoneId: 200050, name: 'Nhân vật'),
        ZoneList(zoneId: 200051, name: 'Sàn diễn'),
      ],
    ),
    ZoneList(
      zoneId: 10,
      name: "Giải trí",
      childZone: [
        ZoneList(zoneId: 1581, name: 'Nghe gì hôm nay'),
        ZoneList(zoneId: 57, name: 'Điện ảnh'),
        ZoneList(zoneId: 58, name: 'Âm nhạc'),
        ZoneList(zoneId: 385, name: 'TV Show'),
        ZoneList(zoneId: 919, name: 'Thời trang'),
        ZoneList(zoneId: 922, name: 'Hậu trường'),
      ],
    ),
    ZoneList(
      zoneId: 1209,
      name: "Thể thao",
      childZone: [
        ZoneList(zoneId: 200030, name: 'Tin tức'),
        ZoneList(zoneId: 200031, name: 'Cộng đồng'),
        ZoneList(zoneId: 200032, name: 'Bình luận'),
        ZoneList(zoneId: 200033, name: 'Video'),
        ZoneList(zoneId: 200034, name: 'Thể thao vui'),
        ZoneList(zoneId: 200057, name: 'SEA Games 30'),
        ZoneList(zoneId: 200042, name: 'World Cup 2018'),
      ],
    ),
    ZoneList(
      zoneId: 13,
      name: "Giáo dục",
      childZone: [
        ZoneList(zoneId: 142, name: 'Tuyển sinh'),
        ZoneList(zoneId: 85, name: 'Du học'),
        ZoneList(zoneId: 913, name: 'Câu chuyện giáo dục'),
        ZoneList(zoneId: 1507, name: 'Học đường'),
        ZoneList(zoneId: 200020, name: 'Góc học tập'),
        ZoneList(zoneId: 200054, name: 'Bài giải – Đáp án'),
      ],
    ),
    ZoneList(
      zoneId: 661,
      name: "Khoa học",
      childZone: [
        ZoneList(zoneId: 200010, name: 'Thường thức'),
        ZoneList(zoneId: 200011, name: 'Phát minh'),
      ],
    ),
    ZoneList(
      zoneId: 12,
      name: "Sức khỏe",
      childZone: [
        ZoneList(zoneId: 200008, name: 'Dinh dưỡng'),
        ZoneList(zoneId: 1465, name: 'Biết để khỏe'),
        ZoneList(zoneId: 197, name: 'Mẹ & Bé'),
        ZoneList(zoneId: 231, name: 'Phòng mạch'),
        ZoneList(zoneId: 241, name: 'Giới tính'),
      ],
    ),
    ZoneList(
      zoneId: 200015,
      name: "Giả - Thật",
      childZone: [],
    ),
    ZoneList(
      zoneId: 118,
      name: "Bạn đọc làm báo",
      childZone: [
        ZoneList(zoneId: 626, name: 'Phản hồi'),
        ZoneList(zoneId: 937, name: 'Đường dây nóng'),
        ZoneList(zoneId: 940, name: 'Chia sẻ'),
        ZoneList(zoneId: 1582, name: 'Đọc báo cùng bạn'),
        ZoneList(zoneId: 1360, name: 'Tiêu điểm'),
      ],
    ),
  ];

  @override
  Widget build(BuildContext context) {
    print('Test build SideMenu');
    TextStyle _textStyle = TextStyle(
      fontFamily: 'SFProDisplayBold',
      fontSize: 16,
    );

    return ListView(
      children: <Widget>[
        Column(
          children: cates.map((itemGroup) {
            return ExpandableGroup(
              header: InkWell(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: Text('${itemGroup.name}', style: _textStyle),
                ),
                onTap: () {},
              ),
              items: itemGroup.childZone
                  .map(
                    (item) => ListTile(
                      contentPadding: const EdgeInsets.only(left: 32),
                      title: Text(
                        item.name,
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      ),
                      onTap: () {},
                    ),
                  )
                  .toList(),
            );
          }).toList(),
        ),
      ],
    );
  }
}
