import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Text size
const double TEXT_SIZE_DEFAULT = 0;
const double TEXT_SIZE_MIN = -2;
const double TEXT_SIZE_MAX = 6;

class SettingFontSilderWidget extends StatelessWidget {
  const SettingFontSilderWidget({
    Key key,
    @required this.value,
    this.textStyle,
    this.onChanged,
  }) : super(key: key);
  final double value;
  final TextStyle textStyle;
  final Function(double newValue) onChanged;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text('A',
            style: textStyle?.copyWith(
              fontSize: textStyle.fontSize + TEXT_SIZE_MIN,
            )),
        Expanded(
          child: Slider(
            value: value ?? TEXT_SIZE_MIN,
            min: TEXT_SIZE_MIN,
            max: TEXT_SIZE_MAX,
            divisions: 8,
            onChanged: onChanged,
          ),
        ),
        Text(
          'A',
          style: textStyle?.copyWith(
            fontSize: textStyle.fontSize + TEXT_SIZE_MAX,
          ),
        ),
      ],
    );
  }
}
