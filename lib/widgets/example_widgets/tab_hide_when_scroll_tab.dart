import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart';
import 'package:flutter/material.dart' hide NestedScrollView;

class TabHideWhenScroll extends StatefulWidget {
  const TabHideWhenScroll({Key key, this.tabKey, this.tabName})
      : super(key: key);
  final Key tabKey;
  final String tabName;

  @override
  _TabHideWhenScrollState createState() => _TabHideWhenScrollState();
}

class _TabHideWhenScrollState extends State<TabHideWhenScroll>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    print('Test build home');
    return Builder(
      builder: (BuildContext context) {
        return NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification scrollNotification) {
            return true;
          },
          child: NestedScrollViewInnerScrollPositionKeyWidget(
            widget.tabKey,
            CustomScrollView(
              slivers: <Widget>[
                SliverOverlapInjector(
                  handle:
                      NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                ),
                SliverPadding(
                  padding: EdgeInsets.zero,
                  sliver: SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        return Container(
                          padding: const EdgeInsets.all(10),
                          height: 50,
                          child: Text('${widget.tabName} - $index'),
                        );
                      },
                      childCount: 55,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
