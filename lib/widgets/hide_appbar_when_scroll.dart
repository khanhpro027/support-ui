import 'dart:io';

import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart';
import 'package:flutter/material.dart' hide NestedScrollView;
import 'package:supportui/support_ui.dart';
import 'package:supportui/widgets/example_widgets/tab_hide_when_scroll_tab.dart';

class HideAppBarWhenScroll extends StatefulWidget {
  const HideAppBarWhenScroll({Key key}) : super(key: key);

  @override
  _HideAppBarWhenScrollState createState() => _HideAppBarWhenScrollState();
}

class _HideAppBarWhenScrollState extends State<HideAppBarWhenScroll>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  List<String> _categories = ['Tab 1', 'Tab 2'];
  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: _categories.length);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        floatHeaderSlivers: true,

        // floatHeaderSlivers is true : is scroll down is show (every where)
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          //<-- headerSliverBuilder
          return <Widget>[
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SliverSafeArea(
                top: false,
                bottom: Platform.isIOS ? false : true,
                sliver: SliverAppBar(
                  title: Text('Title'),
                  actions: [
                    // icon action
                    IconButton(
                      icon: const Icon(Icons.search),
                      onPressed: () {},
                    ),
                    IconButton(
                      icon: const Icon(Icons.settings),
                      onPressed: () {},
                    ),
                  ],
                  pinned: true, //<-- pinned to true
                  floating: true, //<-- floating to true
                  //snap: true,
                  forceElevated: innerBoxIsScrolled,
                  bottom: new TabBar(
                    controller: _tabController,
                    tabs: _categories.map((e) => Tab(text: e)).toList(),
                  ),
                ),
              ),
            ),
          ];
        },
        innerScrollPositionKeyBuilder: () {
          return Key('Tab${_tabController.index}');
        },
        body: new TabBarView(
          controller: _tabController,
          children: _categories.asMap().entries.map((entry) {
            return TabHideWhenScroll(
              tabKey: Key('Tab${entry.key}'),
              tabName: entry.value,
            );
          }).toList(),
        ),
      ),
      drawer: TabBarUI.drawOfTabbar(),
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}
