import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:supportui/widgets/bottom_navigation_bar_widget.dart';
import 'package:supportui/widgets/example_widgets/draw_left_menu.dart';
import 'package:supportui/widgets/example_widgets/seting_font_slider_widget.dart';
import 'package:supportui/widgets/expandable_group.dart';
import 'package:supportui/widgets/hide_appbar_when_scroll.dart';
import 'package:supportui/widgets/switch_widget.dart';
import 'package:tinycolor/tinycolor.dart';

class ButtonUI {
  static Widget leftIconAndText(
      {TypeUI type = TypeUI.simple,
      String text = 'left icon button',
      Widget icon,
      double width,
      double height,
      Color color = Colors.white,
      Color backgroundColor = Colors.lightBlue,
      double radius = 10,
      VoidCallback onPressed}) {
    return type == TypeUI.simple
        ? Container(
            width: width ?? double.maxFinite,
            height: height ?? null,
            child: RaisedButton.icon(
                onPressed: () => onPressed(),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(radius))),
                label: Text(text, style: TextStyle(color: color)),
                icon: icon ?? Icon(Icons.camera_alt, color: color),
                textColor: color,
                color: backgroundColor),
          )
        : Container(
            width: width ?? double.maxFinite,
            height: height ?? null,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(radius))),
              onPressed: () => onPressed(),
              textColor: color,
              color: backgroundColor,
              padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
              child: Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    icon,
                    Expanded(
                      child: Text(
                        text,
                        style: TextStyle(
                            color: color, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
  }

  static Widget textAndRightIcon() {
    return Container(
      width: 210,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        onPressed: () {
          print('Button Clicked.');
        },
        textColor: Colors.white,
        color: Colors.pink,
        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
        child: Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                color: Colors.pink,
                padding: EdgeInsets.fromLTRB(10, 4, 4, 4),
                child: Text(
                  'Button With Right Icon',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
                child: Icon(
                  Icons.backup,
                  color: Colors.white,
                  size: 30,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static Widget simpleText(
      {String text = 'left icon button',
      double width,
      double height,
      Color color = Colors.white,
      Color backgroundColor = Colors.lightBlue,
      double radius = 10,
      VoidCallback onPressed}) {
    return Container(
      width: width ?? null,
      height: height ?? null,
      child: RaisedButton(
        onPressed: () => onPressed != null ? onPressed() : null,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(radius))),
        textColor: color,
        color: backgroundColor,
        child: Text(
          text,
          style: TextStyle(color: color),
        ),
      ),
    );
  }
}

class ImageUI {
  static Widget imageAsset(
      {String path = 'assets/appicon.png', double height, double width}) {
    return SizedBox(height: height, width: width, child: Image.asset(path));
  }
}

class IconUI {
  static Widget iconText(
      {Widget icon, String text = '', VoidCallback onPressed}) {
    return GestureDetector(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [icon, if (text.isNotEmpty) Text(text)],
      ),
      onTap: () => onPressed(),
    );
  }

  static Widget textIcon({String path = ''}) {
    return Image.asset(path);
  }

  static Widget iconOnCircle({Widget icon, Color colorCircle = Colors.white}) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: icon,
      decoration: BoxDecoration(
        color: colorCircle,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
              offset: const Offset(0.0, 2.0),
              color: Colors.grey[350],
              blurRadius: 5.0,
              spreadRadius: 1),
        ],
      ),
    );
  }

  static Widget iconTopTextBottom(
      {Widget icon,
      String title = 'title',
      Color colorText = Colors.black,
      double sizeText = 14.0}) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            (icon == null) ? Icon(Icons.add, color: Colors.black) : icon,
            Text(
              title,
              style: TextStyle(fontSize: sizeText, color: colorText),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }
}

class LoadingUI {
  static Widget appLoadingIndicator() {
    return Center(
      child: Platform.isIOS
          ? const CupertinoActivityIndicator()
          : const CircularProgressIndicator(strokeWidth: 2),
    );
  }
}

class PopupUI {
  static Future<void> showPopup(
      {BuildContext context, Widget widget, bool dismissible = true}) {
    return showDialog<void>(
      barrierDismissible: dismissible,
      context: context,
      builder: (BuildContext context) {
        return widget;
      },
    );
  }

  /// Display normal prompt dialog
  static Future<bool> prompt({
    @required String message,
    String title,
    String acceptText,
    String cancleText,
    VoidCallback onOk,
    VoidCallback onCancel,
    BuildContext context,
    TextStyle textStyle,
  }) async {
    final usedContext = context;
    final usedNavigator = Navigator.of(usedContext, rootNavigator: true);

    final TextStyle style = textStyle ?? const TextStyle(height: 1.3);
    final VoidCallback accept = () => usedNavigator.pop<bool>(true);
    final VoidCallback dismiss = () => usedNavigator.pop<bool>(false);

    bool result;

    if (Platform.isIOS) {
      result = await showCupertinoDialog<bool>(
            context: usedContext,
            useRootNavigator: true,
            builder: (BuildContext context) {
              return CupertinoAlertDialog(
                title: title != null
                    ? Text(title, style: style, textAlign: TextAlign.center)
                    : null,
                content: Text(message, style: style),
                actions: <Widget>[
                  CupertinoDialogAction(
                    isDestructiveAction: true,
                    child: Text(cancleText ?? 'No'),
                    onPressed: dismiss,
                  ),
                  CupertinoDialogAction(
                    child: Text(acceptText ?? 'Yes'),
                    onPressed: accept,
                  ),
                ],
              );
            },
          ) ??
          false;
    } else {
      result = await showDialog<bool>(
            context: usedContext,
            barrierDismissible: false,
            useRootNavigator: true,
            builder: (BuildContext context) {
              return AlertDialog(
                title: title != null
                    ? Text(title, style: style, textAlign: TextAlign.center)
                    : null,
                content: Text(message, style: style),
                actions: <Widget>[
                  FlatButton(
                    child: Text(cancleText ?? 'いいえ'),
                    onPressed: dismiss,
                  ),
                  FlatButton(
                    child: Text(acceptText ?? 'はい'),
                    onPressed: accept,
                  ),
                ],
              );
            },
          ) ??
          false;
    }

    if (result && onOk != null) {
      onOk();
    }

    if (!result && onCancel != null) {
      onCancel();
    }

    return result;
  }
}

class GridViewUI {
  static Widget headerGrid({List<String> listHeader, List<String> listTitle}) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: listHeader.length,
      itemBuilder: (context, index) {
        return StickyHeader(
          header: Container(
            height: 38.0,
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            alignment: Alignment.centerLeft,
            child: Text(
              listHeader[index],
              style: const TextStyle(
                  color: Colors.blue,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
          content: Container(
            color: Colors.white,
            child: GridView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: listTitle.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 1.3,
              ),
              itemBuilder: (contxt, indx) {
                return Card(
                  color: Colors.white,
                  //elevation: 5.0,
                  margin: indx.isEven
                      ? EdgeInsets.only(
                          top: 5.0, bottom: 5.0, left: 10.0, right: 5.0)
                      : EdgeInsets.only(
                          top: 5.0, bottom: 5.0, right: 10.0, left: 5.0),
                  shape: RoundedRectangleBorder(
                      side: BorderSide(width: 1.0, color: Colors.blue)),
                  child: IconUI.iconTopTextBottom(
                    icon:
                        ImageUI.imageAsset(path: 'assets/pin.png', height: 50),
                    title: listTitle[indx],
                    sizeText: 18.0,
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }
}

class ListViewUI {
  static Widget expandListView({List<List<String>> data}) {
    return ListView(
      children: <Widget>[
        Column(
          children: data.map((group) {
            int index = data.indexOf(group);
            return ExpandableGroup(
              header: Text(
                'Group $index',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              items: group
                  .map((e) => ListTile(
                        title: Text(e),
                        leading: Icon(Icons.remove),
                      ))
                  .toList(),
            );
          }).toList(),
        )
      ],
    );
  }

  // Gạch phân chia giửa các item
  static Widget separatedListView() {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.black,
      ),
      itemCount: 20,
      itemBuilder: (context, index) => Padding(
        padding: EdgeInsets.all(8.0),
        child: Center(child: Text("Index $index")),
      ),
    );
  }
}

class SystemUI {
  static Widget blockDoubleBackWillPop({Widget child}) {
    return WillPopScope(onWillPop: () async => false, child: child);
  }
}

class BackgroudUI {
  static Widget gradientBackGroud({Widget child, Color color = Colors.blue}) {
    final TinyColor baseColor = TinyColor(color);
    final bool isLight = baseColor.getBrightness() > 200;
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: isLight
              ? [
                  baseColor.darken(0).color,
                  baseColor.darken(10).color,
                  baseColor.darken(20).color,
                ]
              : [
                  baseColor.lighten(40).color,
                  baseColor.lighten(10).color,
                  baseColor.lighten(00).color,
                ],
        ),
      ),
      child: child,
    );
  }
}

class TabBarUI {
  static Widget hideAppBarWhenScroll() {
    return HideAppBarWhenScroll();
  }

  static Widget drawOfTabbar() {
    return Drawer(child: SideMenuWidget());
  }
}

class BottomNavigationBar {
  static Widget bottomNavigationBarWidget() {
    return BottomNavigationBarWidget();
  }
}

class SwitchUI {
  static Widget switchWidget() {
    return SwitchWidget();
  }
}

class SliderUI {
  static Widget sliderWidget() {
    return SettingFontSilderWidget(value: 3);
  }
}

enum TypeUI { simple, advanced }
