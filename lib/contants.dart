/// Format day
const UI_DATE_FM = r'yyyy年MM月dd日';
const UI_DATE_MMDD = r'MM/dd';
const UI_DATETIME_FM = r'yyyy年MM月dd日 HH:mm:ss';
const UI_DATETIME_HHMM_FM = r'yyyy年MM月dd日 HH:mm';
